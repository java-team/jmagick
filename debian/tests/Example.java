// Copyright 2024 Pierre Gruet <pgt@debian.org>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this package; if not, see https://www.gnu.org/licenses/.

import java.io.*;
import magick.*;

class Example {
	public static void main(String args[]) throws MagickException, IOException {
        // Loading input image
		ImageInfo orig = new ImageInfo("in.jpg");
		MagickImage imageNew = new MagickImage(orig);

        // Defining the context: opacity, font, ...
		DrawInfo context = new DrawInfo(orig);
		context.setFill(PixelPacket.queryColorDatabase("blue"));
		context.setPointsize(28);
		context.setOpacity(95);
		context.setText("Example");
		context.setGeometry("+30+30");
		imageNew.annotateImage(context);

        // Saving image.
		imageNew.setFileName("out.jpg");
		imageNew.writeImage(orig); 
	}
}
